'use client'

import { useQuery } from '@tanstack/react-query'
import React from 'react'
import { useState } from 'react'

import CharacterCard from '@/components/ui/character-card';
import Header from '@/components/ui/header';
import SiteFooter from '@/components/layouts/site-footer';
import { Button } from '@/components/ui/button';


export default function Page({ searchParams }: [ searchParams: [page: number] ]) {
  const [page, setPage] = useState(1)
//  const [data, setData] = useState([])


const getCharacters = async({queryKey}) => {
  const [_, page] = queryKey;
  return fetch( `https://rickandmortyapi.com/api/character/?page=${page}`).then((res) => res.json(),);
}

 const {
  data: characters, isLoading, isError
  } = useQuery({
  queryKey: ['Characters', page ],
  //queryFn: () => getCharacterData(searchParams.page)
  queryFn: getCharacters,
  keepPreviousData: true
  })
  
  if(isLoading){
    return <span className="text-green-350">Loading...</span>
  }
  if(isError){
    return <span className="text-green-350">Помилка...</span>
  }
 
const setPrevPage =() => setPage(page - 1)

const setNextPage =() => setPage(page + 1)

  return (
    <>
      <Header />
      <div className=" flex flex-col justify-between items-center"> 
        <div className="grid grid-cols-4 gap-6">
          {
            characters?.results.map((char: Character) => (
              <CharacterCard 
                key={char.id}
                id={char.id}
                name={char.name}
                status={char.status} 
                species={char.species}
                image={char.image} 
              />
            ))
          }
        </div>
        <div className="flex">
          <Button onClick={() => setPrevPage()}  disabled={page <= 1} className="m-3 w-80 size-medium shape-rounded bg-green-350 
              p-2 text-black font-semibold font-ibmplex text-2xl">
              Prev Page</Button>

          <Button onClick={() => setNextPage()}  disabled={page >= 42} className="m-3 w-80 size-medium shape-rounded bg-green-350 
              p-2 text-black font-semibold font-ibmplex text-2xl">
              Next Page</Button>
        </div> 
      </div>  
      <SiteFooter />
    </>
  )
}