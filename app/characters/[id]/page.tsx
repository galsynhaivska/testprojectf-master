'use client'

import { useQuery } from '@tanstack/react-query' 
import React from 'react'

import SiteFooter from '@/components/layouts/site-footer'
import Header from '@/components/ui/header'
import { Shell } from '@/components/shells/shell'

async function getCharacterbyId(id: number): Promise<Character> {
  const res = await fetch(`https://rickandmortyapi.com/api/character/${id}`)
  if(!res.ok){
    throw new Error("Failed in fetch data")
  }  
  return await res.json()
}

export default function Page({ params }: {params: {id: number } }) {
    const {data: character, isLoading, isError} = useQuery({
        queryKey: ['character', params.id],
        queryFn: () => getCharacterbyId(params.id)
    })

    if(isLoading){
        return <span>Loading...</span>
    }
    if(isError){
        return <span>Помилка...</span>
    }   

  return (
    <>
      <Header />
        <h1 className="text-green-350 font-semibold font-ibmplex text-4xl text-center m-10">{character?.name}</h1>
      <SiteFooter />
    </>
    
  )
}
