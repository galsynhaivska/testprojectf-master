
import Image from 'next/image'
import prehome from '@/components/images/prehome.png'
import logo from '@/components/images/logo.png'

export default function Home() {
	return (
		<>
			<div className="flex flex-col justify-between items-center gap-10">	
          		<Image src={logo} alt={'Logo image'} className="w-[520px] pt-4" /> 
				<Image src={prehome} alt={'Prehome image'} className="w-[638px] object-cover pt-16" />		
			</div>
			
		</>
	)
}
