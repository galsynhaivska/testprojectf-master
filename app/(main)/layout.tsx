import React from 'react'
import SiteFooter from '@/components/layouts/site-footer'

export default async function MainLayout({ children }: { children: React.ReactNode }) {
	return (
		<div className="relative max-w-screen min-h-screen flex flex-col
				bg-space">
				<main className="flex-1">{children}</main>
			<SiteFooter />
		</div>
	)
}
