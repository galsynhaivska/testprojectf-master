import Link from 'next/link'
import React from 'react'
import Image from 'next/image'
import logo from '@/components/images/logo.png'

export default function Header() {
  return (
    <>
    <div className="flex flex-row justify-between px-10 py-5 bg-opacity-0">
      <div className="w-[250px] hover:scale-105">
        <Link href="/" className="font-black text-xl">  
          <Image src={logo} alt={'Character image'} /> 
        </Link>
      </div>
        <ul className="flex flex-row justify-between items-center gap-8
          font-ibmplex text-green-350 text-xl" >
              <li className="hover:underline hover:scale-105">
                <Link href="/" >Home</Link>
              </li>
              <li className="hover:underline hover:scale-105">
                  <Link href="/characters">Characters</Link>
              </li>
              <li className="hover:underline hover:scale-105">
                  <Link href="/planets">Planets</Link>
              </li>
              <li className="hover:underline hover:scale-105">
                  <Link href="/episodes">Episodes</Link>
              </li>
            </ul>
      </div>
    </>

  )
}
