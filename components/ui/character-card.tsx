import React from 'react'
import Link from 'next/link'
import Image from 'next/image'

type CharacterProps = {
    id: number
    name: string
    status: string
    species: string
    image: string
}

export default function CharacterCard({id, name, status, species, image}: CharacterProps) {
  return (
    <Link href={`/characters/${id}`} >
    <div className="bg-green-350 rounded border hover:scale-105">
        <div className="relative h-[410px] w-fill">
            <Image src={image} alt={'Character image'} fill sizes="250px" 
            className="object-cover rounded pb-4 hover:opacity-80" /> 
        </div>
        <div className="flex flex-col gap-2 font-ibmplex text-2xl">
            <div className="h-12">
                <span  className="p-2 text-black font-semibold hover:underline">Name: </span>     
                <span className=" p-2 hover:underline hover:font-semibold"> {name}</span>   
            </div>
            <div className="h-12">
                 <span className="px-2 text-black font-semibold hover:underline">Status: </span>
                 <span className="p-2">{status}</span>
            </div>
            <div className="h-12">
                 <span className="p-2 text-black font-semibold hover:underline">Species: </span>
                 <span className="p-1">{species}</span>
            </div>    
        </div>           
    </div>
    </Link>
  )
}
