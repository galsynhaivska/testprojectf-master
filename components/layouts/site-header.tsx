import Link from 'next/link'
import React from 'react'

export default function SiteHeader() {
	return (
		<>
			<div className="px-10 flex items-center h-[80px] border-b">
				<Link href="/" className="text-blue-500 hover:underline hover:text-blue-600">
					SiteHeader
				</Link>
			</div>
		</>
	)
}
