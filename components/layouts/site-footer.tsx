import React from 'react'
import Link from 'next/link'
import Image from 'next/image'

import footerLogo from '@/components/images/footer-logo.png'
import github from '@/components/images/github.png'

export default function SiteFooter() {
	return (
	<div className="flex justify-between items-center gap-30 h-[106px] p-10 bg-black">
		<div className="flex justify-between items-center gap-3 ">
			<div className="w-[250px] h-[90px] hover:scale-110">
				<Link href="/" className="hover:scale-1">  
					<Image src={footerLogo} alt={'Logo image'} className="w-[250px] w-hover" /> 
				</Link>   
			</div>
			<div className="w-[90px] rotate-90 border border-white bg-white"></div>
			<ul className="flex items-center gap-5 text-white text-base p-4 ">
				<li className="hover:underline hover:scale-105">
					<Link href="/" >Home</Link>
				</li>
				<li className="hover:underline hover:scale-105">
					<Link href="/characters">Characters</Link>
				</li>
				<li className="hover:underline hover:scale-105">
					<Link href="/planets">Planets</Link>
				</li>
			</ul>
		</div>
		<div className="flex flex-col justify-center items-center gap-3 text-white text-l">
			<div > Developers GitHub</div>
			<div className="flex justify-between items-center gap-4">
				<Link href="https://gitlab.com/galsynhaivska/testprojectf-master.git/" >  
					<Image src={github} alt={'GitHub icon'} className="hover:scale-105"  /> 
				</Link>   
				<Link href="/" >  
					<Image src={github} alt={'GitHub icon'} className="hover:scale-105" /> 
				</Link>   
				<Link href="/" >  
					<Image src={github} alt={'GitHub icon'}  className="hover:scale-105"/> 
				</Link>   
			</div>
		</div>
		</div>
		
	)
}
